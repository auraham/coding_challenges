# bs_sqrt.py

def bs_sqrt(n, lb=0, ub=None, guess=1, c=0, eps=0.000001):
    
    
    
    # check max attemps
    if c > 100:
        return guess
        
    # check upper bound
    if ub is None:
        ub = n
        
    # get new guess
    guess  = (ub + lb) / 2
    result = guess*guess
    
    # debug
    print("lb: %.2f, ub: %.2f, g: %.2f, result: %.2f, diff: %.5f" % (lb, ub, guess, result, abs(result - n)))
    #import ipdb; ipdb.set_trace()
    
    if abs(result - n) < eps:
        print("c", c)
        return guess
        
    if result > n:
        
        # decrease upper bound
        ub = guess
        
        bs_sqrt(n, lb, ub, guess, c+1)
    
    if result < n:
        
        # increase lower bound
        lb = guess
        
        bs_sqrt(n, lb, ub, guess, c+1)
    
   
    
    
    
bs_sqrt(90)
    
