// is_unique.java
public class is_unique {

    static boolean isUniqueChars(String str) {
        
        // check length
        if (str.length() > 128) return false;
        
        boolean[] char_set = new boolean[128];
        
        for (int i=0; i < str.length(); i++) {
            int val = str.charAt(i);
            if (char_set[val]) {
                return false;
            }
            char_set[val] = true;
        }
        return true;
    }

    public static void main(String[] args) {
        
        String str = "abcdee";
        System.out.format("%s %s\n", str, isUniqueChars(str));
        
    }
}
