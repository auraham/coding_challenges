# is_unique.py

def is_unique(str):
    
    limit = 128
    charset = [0 for i in range(limit)]
    
    if len(str) > limit:
        return False
        
    for letter in str:
        
        val = ord(letter)
        
        if charset[val]:
            return False
            
        charset[val] = 1
        
    return True

def is_unique_dict(str):
    """
    Same as above, but with dictionary
    """
    
    limit = 128
    charset = { chr(val): 0  for val in range(limit)}
    
    if len(str) > limit:
        return False
        
    for letter in str:
        
        if charset[letter]:
            return False
            
        charset[letter] = 1
        
    return True


for str in ("abcde", "abcdee"):
    print("%s %s" % (str, is_unique(str)))
    print("%s %s" % (str, is_unique_dict(str)))
