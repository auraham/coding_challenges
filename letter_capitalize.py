# letter_capitalize.py

def LetterCapitalize(str):
    
    parts = str.split()
    
    for i in range(len(parts)):
        parts[i] = parts[i].capitalize()
        
    new_str = " ".join(parts)
    
    
    """
    parts = str.split()
    return " ".join([part.capitalize() for part in parts])
    """
    
    return new_str
    
print(LetterCapitalize("i ran there"))
