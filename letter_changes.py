# letter_changes.py

def LetterChanges(str):
    
    str_new = ""
    
    for letter in str:
        
        letter_new = letter         # same letter, just in case
        letter_val = ord(letter)    # ascii value
        
        # is letter in [a,z]?
        if letter_val in range(ord("a"), ord("z")+1):
            
            if letter == "z":
                letter_new = "a"
            else:
                letter_new = chr(letter_val+1)
                
        # is letter in [A,Z]?
        if letter_val in range(ord("A"), ord("Z")+1):
            
            if letter == "Z":
                letter_new = "A"
            else:
                letter_new = chr(letter_val+1)
                
        # change case
        if letter_new.lower() in ("a", "e", "i", "o", "u"):
            letter_new = letter_new.upper()
        
        # save
        str_new += letter_new
        
    return str_new
    

print(LetterChanges("hello*3"))
print(LetterChanges("fun times!"))
