# simple_adding.py

def SimpleAdding(num):
    
    sum = 0
    
    for i in range(1, num+1):
        sum += i
        
    return sum
    
print(SimpleAdding(12))
print(SimpleAdding(140))
