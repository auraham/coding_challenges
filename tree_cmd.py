# tree.py
import sys, os

def find_valid_pairs(h):
    
    n_trees = len(h)
    
    for i in range(n_trees):
        for j in range(i+1, n_trees):
            
            if h[i] <= h[j]:
                print("tuple (i: %d, j: %d) is valid, d=%d" % (i, j, j-i))
    
def find_valid_pairs_best(h):

    best_pair = None        # (pair i, j)
    best_dist = 0           # max dist
    best_j    = 100000      # min j
    
    
    
    n_trees = len(h)
    
    for i in range(n_trees):
        for j in range(i+1, n_trees):
            
            if h[i] <= h[j]:
                print("tuple (i: %d, j: %d) is valid, d=%d" % (i, j, j-i))
                
                # compute dist
                dist = j - i
                
                if dist > best_dist:
                    if j < best_j:
                        # update best solution
                        best_pair = (i, j)
                        best_dist = dist
                        best_j = j
                        
    print("best pair", best_pair)
    print("best dist", best_dist)
    print("best j", best_j)
                
    
if __name__ == "__main__":
    
    counter = 0
    
    for a in sys.stdin:
        print(a)
        counter+=1
        
        if counter == 2:
            break
    
    
