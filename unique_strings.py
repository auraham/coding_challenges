# unique_strings.py

# first
import numpy as np
def get_unique_strings_a(strings):
    return np.unique(strings)
    
# second
def get_unique_strings_b(strings):
    
    items = {}
    
    for key in strings:
        item[key] = 1
    
    return item.keys()
    
# third
def get_unique_strings_c(strings):
    
    cache = []
    
    for item in strings:
        if item in cache:
            cache.append(item)
            
    return cache
    
# fourth
def get_unique_strings_d(strings):
    
    unique = []                         # output
    sorted_strings = sorted(strings)    # sort list before processing it
    
    key = sorted_strings[0]             # add first item in the list
    unique.append(key)
    
    # compare the remaining items
    for item in sorted_strings:
        
        if item != key:
            unique.append(item)
            key = item
    
    return unique
    
    

    
    
if __name__ == "__main__":
    
    text = "...."
    strings = text.split(" ")
    
    
    for f in (get_unique_string_a, get_unique_string_a):
        print(sorted(f(strings)))
    

